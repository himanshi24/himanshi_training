class cart_template {
    constructor() {
        console.log("hi")
        this.init();
    }
    init = () => {
        this.remove_btn_click();
        this.plus_btn_click();
        this.minus_btn_click();
        this.update_btn_click();
    }
    remove_btn_click = () => {
        console.log("cart remove btn")
        var remove_btn = document.querySelectorAll(".remove-btn");
        // var itemqty = document.querySelector(".item-qty").value;
        // var cart_page_wrapper = document.querySelector(".cart-page-wrapper");
        remove_btn.forEach(rb => {
            rb.addEventListener("click",(e) => {
                e.preventDefault();
                var productid =rb.getAttribute('data-id');
                let formData= {
                    'id': productid,
                    'quantity':0
                }
                fetch("/cart/change.js",{
                    method:"POST",
                    headers:{
                        'Content-Type':'application/json'
                    },
                    body:JSON.stringify(formData)
                })
                .then( response => {
                    return response.json();
                })
                .then( response => {
                    console.log(response);
                    console.log("removed succsessfully");
                    // fetch("/cart?view=view")
                    // .then(response => { return response.text();})
                    // .then(response => { console.log(response);
                    //     cart_page_wrapper.innerHTML = response;
                    // })
                })
            })
        })
    }
    plus_btn_click = () => {
        var plus = document.querySelectorAll(".qty-plus");
        plus.forEach(p => {
            p.addEventListener("click",(e) => {
                var x = p.nextElementSibling.value;
                p.nextElementSibling.value = parseInt(x) + 1;                
                var productid = p.getAttribute('data-id');
                let formData = {
                    'id':productid,
                    'quantity': p.nextElementSibling.value
                }
                fetch("/cart/change.js",{
                    method:'POST',
                    headers: {
                        'Content-Type':'application/json'
                    },
                    body:JSON.stringify(formData)
                })
                .then(response => {
                    return response.json();
                })
                .then(response => {
                    console.log(response);
                })
            }) 
        })
    }
    minus_btn_click = () => {
        var minus = document.querySelectorAll(".qty-minus");
        minus.forEach(m => {
            m.addEventListener("click",(e)=> {
                var y = m.previousElementSibling.value;
                m.previousElementSibling.value = parseInt(y) - 1;
                var productid= m.getAttribute('data-id');
                let formData = {
                    'id': productid,
                    'quantity' : m.previousElementSibling.value
                }
                fetch("/cart/change.js",{
                    method : 'POST',
                    headers : {
                        'Content-Type':'application/json'
                    },
                    body : JSON.stringify(formData)
                })
                .then(response => {
                    return response.json();
                })
                .then(response => {
                    console.log(response);   
                })
            })
        })
    }
    update_btn_click = () => {
        document.querySelector(".update_btn").addEventListener("click",() => {
            location.reload();
        })
    }    
}
if(document.querySelector('#shopify-section-cart-template')) {
    new cart_template;
}

