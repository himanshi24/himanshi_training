class loginform {
    constructor() {
        this.init();
    }
    init = () => {
        this.onclick();
    };
    onclick = () => {
        console.log("login_form");
        var message=document.querySelector('.message');
        var uname=document.querySelector('#username');
        var pswd=document.querySelector('#pswd');
        var letter=document.querySelector('.letter');
        var capital=document.querySelector('#capital');
        var number=document.querySelector('#number');
        var length=document.querySelector('#length');
        var submit_btn=document.querySelector('#submit-btn-wrapper');
        submit_btn.addEventListener("click",(e) => {
            if(pswd.value != '')
            {
                if(!pswd.value.match(/[a-z]/g))
                {
                    alert("Must have a lowercase letter");
                    e.preventDefault(); // e.preventDefault() : This function stops the page from reloading or refreshing.
                       //if password is wrong an alert will pop when we close it whole page will refresh and we need to fill everything in the form again to avoid this we use e.preventDefault() function
                }
                else if(!pswd.value.match(/[A-Z]/g))
                {
                    alert("Must have a capital letter");
                    e.preventDefault();  
                }
                else if(!pswd.value.match(/[0-9]/g))
                {
                    alert("Must have a number");
                    e.preventDefault();
                }
                else if(pswd.value.length < 8)
                {
                    alert("Value should be greater than or equal to 8");
                    e.preventDefault();
                }   
            }    
        })
        pswd.addEventListener("click",() => {
            message.style.display="block";
            pswd.onblur = function() {
                message.style.display = "none";
            }
            pswd.onkeyup = function() {
                // Validate lowercase letters
                var lowerCaseLetters = /[a-z]/g;
                if(pswd.value.match(lowerCaseLetters)) {
                  letter.classList.remove("invalid");
                  letter.classList.add("valid");
                } 
                else {
                  letter.classList.remove("valid");
                  letter.classList.add("invalid");
                }
                // Validate uppercase letters
                var uppperCaseLetters = /[A-Z]/g;
                if(pswd.value.match(uppperCaseLetters)) {
                    capital.classList.add('valid');
                    capital.classList.remove('invalid');
                }
                else {
                    capital.classList.remove('valid');
                    capital.classList.add('invalid');
                }
                var numbers = /[0-9]/g;
                if(pswd.value.match(numbers)) {
                    number.classList.add('valid');
                    number.classList.remove('invalid');
                }
                else {
                    number.classList.add('invalid');
                    number.classList.remove('valid');
                }
                if(pswd.value.length >= 8) {
                    length.classList.add('valid');
                    length.classList.remove('invalid');
                }
                else {
                    length.classList.add('invalid');
                    length.classList.remove('valid');
                }
            }
        });    
    }
}
if(document.querySelector(".form-container")) {
new loginform();
}