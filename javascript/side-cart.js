class sideCart {
  constructor() {
    this.init();
  }
  init = () => {
    this.clickEvent();
    this.close_btn();
  };
  cartInit = async () => {
    let res = await fetch("/cart?view=view");
    let resJson = await res.text();
    var side_cart_drawer = document.getElementById("product-side-cart");
    side_cart_drawer.innerHTML = resJson;
    this.clickEvent();
  };
  clickEvent = () => {
    console.log("click function");
    const that = this;
    console.log(document.querySelectorAll(".remove_btn"));
    document.querySelectorAll(".remove_btn").forEach((rb) => {
      rb.addEventListener("click", (e) => {
        console.log("remove clicked");
        e.preventDefault();
        var productid = rb.getAttribute("data-id");
        let formData = {
          id: productid,
          quantity: 0,
        };
        fetch("/cart/change.js", {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify(formData),
        })
        .then((response) => {
          return response.json();
        })
        .then((response) => {
          console.log("product remove successfully");
          console.log(response);
          that.cartInit();
        });
      });
    });
  };
  close_btn = () => {
    document.querySelector(".close-wrapper").addEventListener("click", () => {
      document.querySelector(".side-cart-drawer").style.display = "none";
    });
  };
}
if (document.querySelector(".side-cart-drawer")) {
  console.log("test");
  new sideCart();
}
