class product {
  constructor() {
    this.init();
  }
  init = () => {
    this.changeevent();
    this.submitevent();
  };
  changeevent = () => {
    var variant = document.querySelectorAll(".variant-choose");
    var btn = document.querySelectorAll(".button-label");
    var radios = document.querySelectorAll(".radio-button.size");
    var radioc = document.querySelectorAll(".radio-button.color");
    var qty = document.querySelector(".qtyinput");
    var color = "";
    var size = "";
    var varqty = "";
    radios.forEach((rs) => {
      var temp = rs.nextElementSibling;
      temp.classList.add("size-label");
      // size = rs.value;
      // console.log("all-size" + size);
      if (rs.checked) {
        temp.classList.add("active");
        size = rs.value; //will give the value of checked input size
      } else if (rs.unchecked) {
        temp.classList.remove("active");
      }
      rs.addEventListener("click", function (ev) {
        if (rs.checked) {
          btn.forEach((b) => {
            if (b.classList.contains("size-label")) {
              if (ev.target.nextElementSibling != b) {
                b.classList.remove("active");
              }
              temp.classList.add("active");
            }
          });
        } else {
          temp.classList.remove("active");
        }
      });
    });
    radioc.forEach((rc) => {
      var temp = rc.nextElementSibling;
      temp.classList.add("color-label");
      if (rc.checked) {
        temp.classList.add("active");
        color = rc.value; //will give the value of checked input color
      } else if (rc.unchecked) {
        temp.classList.remove("active");
      }
      rc.addEventListener("click", function (ev) {
        if (rc.checked) {
          btn.forEach((b) => {
            if (b.classList.contains("color-label")) {
              if (ev.target.nextElementSibling != b) {
                b.classList.remove("active");
              }
              temp.classList.add("active");
            }
          });
        } else {
          temp.classList.remove("active");
        }
      });
    });
    radios.forEach((rs) => {
      rs.addEventListener("change", function () {
        // this change event will trigger when we will change the size input from M to L or S to M
        size = this.value; //it will hold the current value of size input button eg: L or M
        console.log("You have changed size - " + size, color); // we will call this function totalVariant(size , color) when we change the input value
        totalVariant(size, color); //we will pass the  changed value of size and existing value of color to function totalVariant(size , color) eg: totalVariant(L , pink)
      });
    });
    radioc.forEach((rc) => {
      rc.addEventListener("change", function () {
        // this change event will trigger when we will change the color input for eg. red to pink or pink to blue
        color = this.value; //it will hold the current value of color input button eg: pink or red
        console.log("You have changed color - " + size, color); //it will print in console eg: L red, M pink
        // we will call this function totalVariant(size , color) when we change the input value
        totalVariant(size, color); //we will pass the  changed value of color and existing value of size to function totalVariant(size , color) eg: totalVariant(L , pink)
      });
    });
    qty.addEventListener("change", function () {
      // this change event will trigger when we will change the quantity input for eg. 1 to 2,2 to 3
      varqty = this.value; //it will hold the current value of quantity
      console.log("You have changed quantity - " + size, color, varqty);
    });
    function totalVariant(size, color) {
      // We got the changed value of size and color radio buttons
      // myCurrentProduct it will give all the information of a product and it is needed to be declared in liquid file
      // myCurrentProduct.variants.length will give the no. of variants eg: 2*3 = 6 , 2 sizes ,3 colors
      // variant is L/pink ; myCurrentProduct.variants.option1 = L ; myCurrentProduct.variants.option2 = pink
      // we will match the option1 and option2 of all the variants to current size and color values once the values are matched then we'll get the product variant id of that variant
      for (var i = 0; i < myCurrentProduct.variants.length; i++) {
        // this loop is for variants and will iterate till myCurrentProduct.variants.length
        if (
          myCurrentProduct.variants[i].option1 == size &&
          myCurrentProduct.variants[i].option2 == color
        ) {
          // eg: if( L == L && red == red) variant will be L/red and we will get its id using myCurrentProduct.variants[i].id
          // Here once we got the same options as size and color we will get the product variant id of that product
          console.log(
            "Variant ID of selected product variant",
            myCurrentProduct.variants[i].id
          );
          document.querySelector("#product-select").value =
            myCurrentProduct.variants[i].id; //we have assigned the product variant id to selection box vlaue eg:selection box vlaue = product variant id
          console.log(
            document.querySelector("#product-select").value,
            'document.querySelector("#product-select")'
          ); // eg:selection box vlaue = product variant id
          // {{variant title}} will be changed according to the select box value which contains product variant id
        }
      }
      //here using the above forloop we will fetch the product variant id of the changed variant and assign it as the value of selection-box from which the variant title will be changed
      // because we have passed {{ variant.title }} in option tag
    }
  };
  cartInit = async () => {
    document.querySelector(".side-cart-drawer").style.display = "block";
  };
  submitevent = () => {
    console.log("test");
    let that = this;
    var addtocartform = document.querySelector("#AddToCartForm");
    var addtocartbtn = document.querySelector(".addtocart-button .btn");
    var qty = document.querySelector(".qtyinput");
    var side_cart_drawer = document.querySelector(".side-cart-drawer");
    qty.addEventListener("change", function () {
      // this change event will trigger when we will change the quantity input for eg. 1 to 2,2 to 3
      varqty = this.value; //it will hold the current value of quantity
    });
    console.log("submitevent");
    addtocartform.addEventListener("submit", (e) => {
      debugger;
      //when we submit the form it will stop the page from refreshing and perform the function
      e.preventDefault();
      addtocartbtn.value = "Added to Cart";
      let formData = {
        id: document.querySelector("#product-select").value,
        quantity: document.querySelector(".qtyinput").value,
      };
      fetch("/cart/add.js", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(formData),
      })
        .then((response) => {
          return response.json();
        })
        .then((response) => {
          // fetch("/cart?view=view")
          // .then(response => {
          //     return response.text();
          // })
          // .then(response => {
          //     document.querySelector("#product-side-cart").innerHTML = response;
          // })
        })
        .then((response) => {
          that.cartInit();
        })
        .catch((error) => {
          console.error("Error:", error);
        });
        function totalVariant(size, color) {
            // We got the changed value of size and color radio buttons 
            // myCurrentProduct it will give all the information of a product and it is needed to be declared in liquid file 
            // myCurrentProduct.variants.length will give the no. of variants eg: 2*3 = 6 , 2 sizes ,3 colors 
            // variant is L/pink ; myCurrentProduct.variants.option1 = L ; myCurrentProduct.variants.option2 = pink
            // we will match the option1 and option2 of all the variants to current size and color values once the values are matched then we'll get the product variant id of that variant
            for (var i = 0; i < myCurrentProduct.variants.length; i++) { // this loop is for variants and will iterate till myCurrentProduct.variants.length
                if ( myCurrentProduct.variants[i].option1 == size && myCurrentProduct.variants[i].option2 == color ) { 
                    // eg: if( L == L && red == red) variant will be L/red and we will get its id using myCurrentProduct.variants[i].id
                    // Here once we got the same options as size and color we will get the product variant id of that product
                        console.log("Variant ID of selected product variant", myCurrentProduct.variants[i].id);
                        document.querySelector("#product-select").value = myCurrentProduct.variants[i].id; //we have assigned the product variant id to selection box vlaue eg:selection box vlaue = product variant id
                        console.log(document.querySelector("#product-select").value,'document.querySelector("#product-select")');// eg:selection box vlaue = product variant id
                    // {{variant title}} will be changed according to the select box value which contains product variant id
                }
            }
            //here using the above forloop we will fetch the product variant id of the changed variant and assign it as the value of selection-box from which the variant title will be changed
            // because we have passed {{ variant.title }} in option tag
        } 
    });
    cartinit = async () => {
        document.querySelector('.side-cart-drawer').style.display = "block"
    }
    cartInit = async () => {
        let res = await fetch('/cart?view=view');
        let resJson = await res.text();
        var side_cart_drawer = document.getElementById('product-side-cart');
        side_cart_drawer.innerHTML = resJson;
        this.clickEvent();
    }
    clickEvent = () => {
        const that = this;
        console.log( document.querySelectorAll(".remove_btn"));
        document.querySelectorAll(".remove_btn").forEach(rb => {
            rb.addEventListener("click",(e) => {
                console.log("remove clicked");
                e.preventDefault();
                var productid = rb.getAttribute('data-id');
                let formData = {
                    'id': productid,
                    'quantity':0
                }
                fetch("/cart/change.js",{
                    method:"POST",
                    headers:{
                        'Content-Type':'application/json'
                    },
                    body:JSON.stringify(formData)
                })
                .then( response => {
                    return response.json();
                })
                .then( response => {
                    console.log("product remove successfully")
                    that.cartInit();
                })
            })
        })
    }
    submitevent = () => {
        let that = this;
        var addtocartform = document.querySelector("#AddToCartForm");
        var addtocartbtn = document.querySelector(".addtocart-button .btn");
        var qty=document.querySelector('.qtyinput');
        var side_cart_drawer = document.querySelector('.side-cart-drawer');
        qty.addEventListener('change', function(){ // this change event will trigger when we will change the quantity input for eg. 1 to 2,2 to 3
            varqty = this.value; //it will hold the current value of quantity
        });
        addtocartform.addEventListener("submit",(e) => {
        //when we submit the form it will stop the page from refreshing and perform the function
            e.preventDefault();
            addtocartbtn.value = "Added to Cart";
            let formData = {
                'id':  document.querySelector("#product-select").value,
                'quantity': document.querySelector('.qtyinput').value
            };
            fetch('/cart/add.js', {
                method: 'POST',
                headers: {
                'Content-Type': 'application/json'
                },
                body: JSON.stringify(formData)
            })
            .then(response => {
                return response.json();
            })
            .then(response => {
                fetch("/cart?view=view")
                .then(response => {
                    return response.text();
                })
                .then(response => {
                    document.querySelector("#product-side-cart").innerHTML = response;
                })
                .then(response => {
                    this.clickEvent();
                })
            })
            .then(response => {
                that.cartinit();
            })
            .catch((error) => {
                console.error('Error:', error);  
            });
            //    fetch('/cart.js').then(response => { return response.json();}).then(response => { console.log(response); })
        })    
    }
}
if (document.querySelector(".template-product")) {
  new product();
}
