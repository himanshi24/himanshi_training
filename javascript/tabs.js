class tabs {
    constructor() {
        this.init();
    }
    init = () => {
        this.tabClick();
    }
    tabClick = ()  => {
        const tabs = document.querySelectorAll('.product-description-tabs .product-btn');
        const tabInfos = document.querySelectorAll('.panel');
        const accBody = document.querySelectorAll(".product-description-tabs .accordian");
        if(tabs.length > 0) {
            tabs.forEach(tab => {
                tab.addEventListener('click', (e) => {
                    e.preventDefault();
                    const matchMedia = window.matchMedia("(max-width : 760px )");
                    if(matchMedia.matches) {
                        tab.classList.toggle("active");
                        var acc = e.target.nextElementSibling;
                        acc.classList.toggle("active");
                        accBody.forEach(body => {
                            if(acc != body) {
                                body.classList.remove('active');
                            }
                        })
                        tabs.forEach(tab => {
                            if(tab != e.target) {
                                tab.classList.remove("active");
                            }
                        })
                    }
                    else {
                        tabs.forEach(tab => {
                            if(tab != e.target) {
                                tab.classList.remove("active");
                            }
                        })
                        tab.classList.add("active");
                        const target = document.querySelector(tab.dataset.tabValue);
                        console.log(target);
                        tabInfos.forEach(tabInfo => {
                            tabInfo.classList.remove('active');
                        })
                        target.classList.add('active');
                    }
                })
            })
        }
    }
}
if(document.querySelector('.description-tab')) {
    new tabs;
}


