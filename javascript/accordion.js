class accordian {
  constructor() {
    this.init();
  }
  init = () => {                                //initial function
    this.on_click();
  };
  on_click = () => {
    const acc_btns = document.querySelectorAll(".accordion_head");
    const acc_body = document.querySelectorAll(".accordion_body");  //will get all the divs having class ".accordion_body" in an array
    acc_btns.forEach((btn) => {                         //for loop here btn is a user defined variable
        btn.addEventListener("click", (e) => {          //click event is performed and a parameter e is passed
             //here e.target will give the next sibling element of the element on click event is performed 
          acc_body.forEach((acc) => { 
            if(e.target.nextElementSibling !== acc && acc.classList.contains('active')) {    //when we click on the button it will take all the <p> tag elements in for loop 
              acc.classList.remove('active')                                                 //and will take the clicked element in e.target and start comparing it with other body given elements
              acc_btns.forEach((btn) => {                                                    //and remove the active class if condition satifies
                btn.classList.remove('active');
              })
            }
          })
          const panel =  btn.nextElementSibling;
          panel.classList.toggle("active");
          btn.classList.toggle("active");
      });
    });
  };
}
new accordian();

